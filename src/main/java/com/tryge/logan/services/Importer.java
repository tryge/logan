package com.tryge.logan.services;

import java.io.File;

/**
 * @author michael.zehender@tryge.com
 */
public interface Importer {
	void importFile(File file);
}
