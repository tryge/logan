package com.tryge.logan.services;

/**
 * @author michael.zehender@me.com
 */
public interface MatchResult {
	boolean isMatch();
}
