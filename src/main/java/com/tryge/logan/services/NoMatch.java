package com.tryge.logan.services;

/**
 * @author michael.zehender@me.com
 */
public class NoMatch implements MatchResult {
	@Override
	public boolean isMatch() {
		return false;
	}
}
