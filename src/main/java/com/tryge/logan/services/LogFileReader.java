package com.tryge.logan.services;

import java.io.File;
import java.io.IOException;

/**
 * @author michael.zehender@me.com
 */
public interface LogFileReader {
	void read(File file) throws IOException;
}
