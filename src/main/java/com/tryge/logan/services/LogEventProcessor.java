package com.tryge.logan.services;

/**
 * @author michael.zehender@me.com
 */
public interface LogEventProcessor {
	void process(LogEvent event);
}
