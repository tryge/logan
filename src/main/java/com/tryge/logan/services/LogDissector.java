package com.tryge.logan.services;

/**
 * @author michael.zehender@me.com
 */
public interface LogDissector {
	void eof();
	void pushLine(String line);
}
