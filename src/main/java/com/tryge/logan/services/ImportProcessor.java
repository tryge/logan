package com.tryge.logan.services;

import java.io.File;

/**
 * @author michael.zehender@tryge.com
 */
public interface ImportProcessor extends LogEventProcessor {
	/**
	 * This method is called when the processing of a file starts.
	 *
	 * @param file that is being processed
	 */
	void file(File file);

	/**
	 * This method is called when the processing of a file has ended (i.e.
	 * the end of file is reached).
	 */
	void eof();
}
