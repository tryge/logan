package com.tryge.logan.services;

/**
 * @author michael.zehender@me.com
 */
public interface LineMatcher {
	MatchResult match(String line);
}
