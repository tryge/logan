package com.tryge.logan.services;

/**
 * @author michael.zehender@me.com
 */
public class LogLineMatch implements MatchResult {
	private String date;
	private String level;
	private String component;
	private String message;

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public String getComponent() {
		return component;
	}

	public void setComponent(String component) {
		this.component = component;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public boolean isMatch() {
		return true;
	}
}
