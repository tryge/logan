package com.tryge.logan.impl;

import com.tryge.logan.services.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * @author michael.zehender@me.com
 */
public class LogDissectorImpl implements LogDissector {
	private LogLineMatch match;
	private List<String> body = new ArrayList<String>();

	@Override
	public void eof() {
		submitForProcessing();
	}

	@Override
	public void pushLine(String line) {
		MatchResult result = lineMatcher.match(line);

		if (result.isMatch()) {
			submitForProcessing();

			match = (LogLineMatch)result;
			body.clear();
		} else {
			body.add(line);
		}
	}

	private void submitForProcessing() {
		if (match != null) {
			LogEvent logEvent = new LogEvent();
			logEvent.setLevel(match.getLevel());
			logEvent.setComponent(match.getComponent());
			logEvent.setMessage(match.getMessage());
			logEvent.setBody(calculateBody());
			try {
				if (match.getDate() != null) {
					logEvent.setDate(dateFormat.parse(match.getDate()));
				}
			} catch (ParseException e) {
				// TODO throw configuration exception
			}

			processor.process(logEvent);
		}
	}

	private String calculateBody() {
		if (body.isEmpty()) {
			return null;
		}

		StringBuilder builder = new StringBuilder();
		builder.append(body.remove(0));

		while (!body.isEmpty()) {
			builder.append("\n")
				.append(body.remove(0));
		}

		return builder.toString();
	}

	/// INJECTED
	private SimpleDateFormat dateFormat;
	private LineMatcher lineMatcher;
	private LogEventProcessor processor;

	public void setDateFormat(String dateFormat) {
		this.dateFormat = new SimpleDateFormat(dateFormat);
	}

	public void setLineMatcher(LineMatcher lineMatcher) {
		this.lineMatcher = lineMatcher;
	}

	public void setProcessor(LogEventProcessor processor) {
		this.processor = processor;
	}
}
