package com.tryge.logan.impl;

import com.tryge.logan.services.LineMatcher;
import com.tryge.logan.services.LogLineMatch;
import com.tryge.logan.services.MatchResult;
import com.tryge.logan.services.NoMatch;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author michael.zehender@me.com
 */
public class LineMatcherImpl implements LineMatcher {
	@Override
	public MatchResult match(String line) {
		Matcher matcher = pattern.matcher(line);

		if (matcher.matches()) {
			LogLineMatch match = new LogLineMatch();

			if (dateGroup > 0)
				match.setDate(matcher.group(dateGroup));
			if (levelGroup > 0)
				match.setLevel(matcher.group(levelGroup));
			if (componentGroup > 0)
				match.setComponent(matcher.group(componentGroup));
			if (messageGroup > 0)
				match.setMessage(matcher.group(messageGroup));

			return match;
		}

		return new NoMatch();
	}

	/// INJECTED
	private Pattern pattern;
	private int dateGroup;
	private int levelGroup;
	private int componentGroup;
	private int messageGroup;

	public void setExpression(String expr) {
		pattern = Pattern.compile(expr);
	}

	public void setDateGroup(int dateGroup) {
		this.dateGroup = dateGroup;
	}

	public void setLevelGroup(int levelGroup) {
		this.levelGroup = levelGroup;
	}

	public void setMessageGroup(int messageGroup) {
		this.messageGroup = messageGroup;
	}

	public void setComponentGroup(int componentGroup) {
		this.componentGroup = componentGroup;
	}
}
