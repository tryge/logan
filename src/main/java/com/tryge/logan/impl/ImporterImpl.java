package com.tryge.logan.impl;

import com.tryge.logan.services.ImportProcessor;
import com.tryge.logan.services.Importer;
import com.tryge.logan.services.LogFileReader;

import java.io.File;
import java.io.IOException;

/**
 * @author michael.zehender@tryge.com
 */
public class ImporterImpl implements Importer {
	@Override
	public void importFile(File file) {
		if (!file.exists()) {
			System.err.println("File: " + file.getAbsolutePath() + " doesn't exist");
			return;
		}

		try {
			importProcessor.file(file);
			logFileReader.read(file);
			importProcessor.eof();
		} catch(IOException ioe) {
			System.err.println("Exception while processing file: " + file.getAbsolutePath());
			ioe.printStackTrace();
		}
	}

	/// INJECTED
	private LogFileReader logFileReader;
	private ImportProcessor importProcessor;

	public void setLogFileReader(LogFileReader logFileReader) {
		this.logFileReader = logFileReader;
	}

	public void setImportProcessor(ImportProcessor importProcessor) {
		this.importProcessor = importProcessor;
	}
}
