package com.tryge.logan.impl;

import com.tryge.logan.services.ImportProcessor;
import com.tryge.logan.services.LogEvent;

import javax.sql.DataSource;
import java.io.File;
import java.sql.*;
import java.util.Calendar;

/**
 * @author michael.zehender@tryge.com
 */
public class DatabaseImportProcessor implements ImportProcessor {
	private File currentFile;
	private long currentFileId;

	@Override
	public void file(File file) {
		currentFile = file;
		currentFileId = processNewFile(currentFile);
	}

	@Override
	public void eof() {
		currentFile = null;
		currentFileId = 0;
	}

	@Override
	public void process(LogEvent event) {
		processLogEvent(currentFileId, event);
	}

	protected long processNewFile(File file) {
		Connection connection = null;
		PreparedStatement statement = null;

		try {
			connection = dataSource.getConnection();
			connection.setAutoCommit(false);

			statement = connection.prepareStatement(insertLogFileSql, Statement.RETURN_GENERATED_KEYS);
			statement.setString(1, file.getPath());
			statement.executeUpdate();
			connection.commit();

			return fetchCurrentFileId(connection, statement, file);
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			close(statement);
			close(connection);
		}
	}

	private long fetchCurrentFileId(Connection connection, PreparedStatement statement, File file) throws SQLException {
		ResultSet resultSet = null;

		try {
			resultSet = statement.getGeneratedKeys();
			if (resultSet.next()) {
				return resultSet.getLong(1);
			} else {
				return selectCurrentFileId(connection, file);
			}
		} finally {
			close(resultSet);
		}
	}

	private long selectCurrentFileId(Connection connection, File file) throws SQLException {
		PreparedStatement statement = null;
		ResultSet resultSet = null;

		try {
			statement = connection.prepareStatement(selectLogFileIdSql);
			statement.setString(1, file.getPath());

			resultSet = statement.executeQuery();
			if (resultSet.next()) {
				return resultSet.getLong(1);
			} else {
				throw new RuntimeException("Can't get the id of the inserted log_file!");
			}
		} finally {
			close(resultSet);
			close(statement);
		}
	}

	protected void processLogEvent(long currentFileId, LogEvent event) {
		Connection connection = null;
		PreparedStatement statement = null;

		try {
			connection = dataSource.getConnection();
			connection.setAutoCommit(false);

			Calendar calendar = Calendar.getInstance();
			calendar.setTime(event.getDate());

			statement = connection.prepareStatement(insertLogEventSql);
			statement.setLong(1, currentFileId);
			statement.setTimestamp(2, new Timestamp(calendar.getTimeInMillis()));
			statement.setInt(3, calendar.get(Calendar.MILLISECOND));
			statement.setString(4, event.getComponent());
			statement.setString(5, event.getMessage());
			statement.setString(6, event.getLevel());
			statement.setString(7, event.getBody());
			statement.executeUpdate();

			connection.commit();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			close(statement);
			close(connection);
		}
	}

	private void close(AutoCloseable closable) {
		try {
			if (closable != null) {
				closable.close();
			}
		} catch (Exception e) {
			// ignore
		}
	}

	/// INJECTED
	private DataSource dataSource;
	private String insertLogFileSql = "INSERT INTO log_files (name) VALUES (?)";
	private String selectLogFileIdSql = "SELECT log_file_id FROM log_files WHERE name = ?";
	private String insertLogEventSql = "INSERT INTO log_events (log_file_id, clock, millis, component, message, level, body) VALUES (?, ?, ?, ?, ?, ?, ?)";

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	public void setInsertLogFileSql(String insertLogFileSql) {
		this.insertLogFileSql = insertLogFileSql;
	}

	public void setSelectLogFileIdSql(String selectLogFileIdSql) {
		this.selectLogFileIdSql = selectLogFileIdSql;
	}

	public void setInsertLogEventSql(String insertLogEventSql) {
		this.insertLogEventSql = insertLogEventSql;
	}
}
