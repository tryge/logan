package com.tryge.logan.impl;

import com.tryge.logan.services.LogDissector;
import com.tryge.logan.services.LogFileReader;

import java.io.*;

/**
 * @author michael.zehender@me.com
 */
public class LogFileReaderImpl implements LogFileReader {
	@Override
	public void read(File file) throws IOException {
		BufferedReader in = new BufferedReader(new FileReader(file));

		for(;;) {
			String line = in.readLine();

			if (line == null) {
				break;
			}

			dissector.pushLine(line);
		}

		dissector.eof();
		in.close();
	}

	/// INJECTED
	private LogDissector dissector;

	public void setLogDissector(LogDissector dissector) {
		this.dissector = dissector;
	}
}
