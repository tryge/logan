package com.tryge.logan;

import com.tryge.logan.services.Importer;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.File;
import java.io.IOException;

public class Logan {
	public static final String PROFILE_PROP = "profile";
	public static final String DEFAULT_PROFILE_VALUE = "default";

	public static void main(String[] args) throws IOException {
		ensureDefaultProfile();
		startApplication(args);

	}

	private static void ensureDefaultProfile() {
		String profile = System.getProperty(PROFILE_PROP);
		if (profile == null) {
			System.setProperty(PROFILE_PROP, DEFAULT_PROFILE_VALUE);
		}
	}

	private static void startApplication(String[] args) {
		AbstractApplicationContext context =
			new ClassPathXmlApplicationContext("applicationContext.xml");
		context.registerShutdownHook();

		Importer importer = context.getBean(Importer.class);

		for (String arg : args) {
			importer.importFile(new File(arg));
		}
	}
}

