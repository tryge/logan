Feature: Import Log File
  In order to analyze my loc records
  I want to import my log file into the database

  Scenario: Single Line Event Import
    Given following log lines
      | line                                                               |
      | 2000-09-07 14:07:41,508 [main] DEBUG MyApp - Entering application. |
      | 2000-09-07 14:07:41,608 [main] INFO  MyApp - Exiting application.  |
      And a default Log4J configuration
    When I import the log file into an emty database
    Then there are 2 log events recorded
     And checking log event "1"
    Then it is dated on "2000-09-07 14:07:41,508"
     And its level is "DEBUG"
     And it is issued by "MyApp"
    And its message is "Entering application."
    And checking log event "2"
    Then it is dated on "2000-09-07 14:07:41,608"
    And its level is "INFO"
    And it is issued by "MyApp"
    And its message is "Exiting application."

