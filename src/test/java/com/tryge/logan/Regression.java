package com.tryge.logan;

import com.tryge.logan.impl.*;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * @author michael.zehender@me.com
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
	SimpleLineMatcherTest.class,
	Log4JLineMatcherTest.class,
	LogDissectorTest.class,
	LogFileReaderTest.class,
	DatabaseImportProcessorTest.class,
	ImporterTest.class
})
public class Regression {
}
