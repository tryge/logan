package com.tryge.logan.glue;

import com.tryge.logan.impl.LineMatcherImpl;
import com.tryge.logan.services.LogLineMatch;
import com.tryge.logan.services.MatchResult;
import cucumber.annotation.en.Given;
import cucumber.annotation.en.Then;
import cucumber.annotation.en.When;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * @author michael.zehender@me.com
 */
@SuppressWarnings("UnusedDeclaration")
public class LineMatcherStepDefs {
	private LineMatcherImpl lineMatcher;
	private MatchResult result;
	private LogLineMatch match;

	@Given("^the LineMatcher service$")
	public void the_line_matcher_service() {
		lineMatcher = new LineMatcherImpl();
	}

	@Given("^it is configured with expr=\"([^\"]*)\"$")
	public void it_is_configured_with_expr(String expr) {
		lineMatcher.setExpression(expr);
	}

	@Given("^it is configured with date=\"([^\"]*)\"$")
	public void it_is_configured_with_date(int date) {
		lineMatcher.setDateGroup(date);
	}

	@Given("^it is configured with level=\"(\\d+)\"$")
	public void it_is_configured_with_level(int level) {
		lineMatcher.setLevelGroup(level);
	}

	@Given("^it is configured with component=\"(\\d+)\"$")
	public void it_is_configured_with_component(int component) {
		lineMatcher.setComponentGroup(component);
	}

	@Given("^it is configured with message=\"(\\d+)\"$")
	public void it_is_configured_with_message(int message) {
		lineMatcher.setMessageGroup(message);
	}

	@When("^I try to match \"([^\"]*)\"$")
	public void I_try_to_match(String line) {
		result = lineMatcher.match(line);
	}

	@Then("^there should be no match$")
	public void there_should_be_no_match() {
		if (result.isMatch())
			fail("there shouldn't be a match");
	}

	@Then("^there should be a match$")
	public void there_should_be_a_match() {
		if (!result.isMatch())
			fail("there should be a match");

		match = (LogLineMatch)result;
	}

	@Then("^the log date should be \"([^\"]*)\"$")
	public void the_log_date_should_be(String date) {
		assertEquals(date, match.getDate());
	}

	@Then("^the log level should be \"([^\"]*)\"$")
	public void the_log_level_should_be(String level) {
		assertEquals(level, match.getLevel());
	}

	@Then("^the log component should be \"([^\"]*)\"$")
	public void the_log_component_should_be(String component) {
		assertEquals(component, match.getComponent());
	}

	@Then("^the log message should be \"([^\"]*)\"$")
	public void the_log_message_should_be(String message) {
		assertEquals(message, match.getMessage());
	}
}
