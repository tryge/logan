package com.tryge.logan.misc;

import com.tryge.logan.impl.LineMatcherImpl;
import com.tryge.logan.services.LineMatcher;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author michael.zehender@me.com
 */
public class Utils {
	public static LineMatcher getLog4JLineMatcher() {
		LineMatcherImpl lineMatcher = new LineMatcherImpl();
		lineMatcher.setExpression("^(\\d{4}\\-\\d{2}\\-\\d{2} \\d{2}:\\d{2}:\\d{2},\\d{3}) \\[[^\\]]+\\] (\\w+)\\s+([^\\s]+) \\- (.*)$");
		lineMatcher.setDateGroup(1);
		lineMatcher.setLevelGroup(2);
		lineMatcher.setComponentGroup(3);
		lineMatcher.setMessageGroup(4);

		return lineMatcher;
	}

	public static String getLog4JDateFormat() {
		return "yyyy-MM-dd hh:mm:ss,SSS";
	}

	public static Date parseLog4JDate(String date) throws ParseException {
		SimpleDateFormat format = new SimpleDateFormat(getLog4JDateFormat());

		return format.parse(date);
	}
}
