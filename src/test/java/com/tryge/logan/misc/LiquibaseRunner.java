package com.tryge.logan.misc;

import liquibase.Liquibase;
import liquibase.database.Database;
import liquibase.database.DatabaseFactory;
import liquibase.database.jvm.JdbcConnection;
import liquibase.exception.DatabaseException;
import liquibase.exception.LiquibaseException;
import liquibase.resource.FileSystemResourceAccessor;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Map;

/**
 * @author michael.zehender@tryge.com
 */
public class LiquibaseRunner {
	private final String changeLog;
	private final Map<String, String> parameters;

	public LiquibaseRunner(String changeLog, Map<String, String> parameters) {
		this.changeLog = changeLog;
		this.parameters = parameters;
	}

	public String getChangeLog() {
		return changeLog;
	}

	public void run(Connection c) throws LiquibaseException {
		Liquibase liquibase = null;
		try {
			liquibase = createLiquibase(c);
			liquibase.update(null);
		} finally {
			if (liquibase != null) {
				liquibase.forceReleaseLocks();
			}
			if (c != null) {
				try {
					c.rollback();
					c.close();
				} catch (SQLException e) {
					//nothing to do
				}
			}
		}

	}

	protected Liquibase createLiquibase(Connection c) throws LiquibaseException {
		Liquibase liquibase = new Liquibase(
			getChangeLog(),
			new FileSystemResourceAccessor(),
			createDatabase(c)
		);

		if (parameters != null) {
			for(Map.Entry<String, String> entry: parameters.entrySet()) {
				liquibase.setChangeLogParameter(entry.getKey(), entry.getValue());
			}
		}

		return liquibase;
	}

	protected Database createDatabase(Connection c) throws DatabaseException {
		return DatabaseFactory.getInstance().findCorrectDatabaseImplementation(new JdbcConnection(c));
	}
}
