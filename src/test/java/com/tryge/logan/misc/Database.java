package com.tryge.logan.misc;

import liquibase.exception.LiquibaseException;
import org.junit.rules.ExternalResource;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * @author michael.zehender@tryge.com
 */
public class Database extends ExternalResource {
	private static final String JDBC_URL = "jdbc:hsqldb:mem:test;shutdown=true";
	private static final String USER = "SA";
	private static final String PASSWORD = "";

	private static final String LOGAN_SCHEMA = "src/main/liquibase/logan-schema-0.1.xml";

	private List<Connection> created = new ArrayList<Connection>(3);

	@Override
	protected void before() {
		try {
			// this connection guarantees that the database remains
			// open until the test case has been executed
			newConnection();

			LiquibaseRunner runner = new LiquibaseRunner(
				LOGAN_SCHEMA,
				new HashMap<String, String>()
			);
			runner.run(newConnection());
		} catch(SQLException sqle) {
			throw new RuntimeException("can't create connection to database");
		} catch (LiquibaseException e) {
			throw new RuntimeException("couldn't create database schema");
		}
	}

	@Override
	protected void after() {
		for (Connection connection : created) {
			try {
				if (!connection.isClosed()) {
					connection.close();
				}
			} catch (SQLException e) {
				// ignore
			}
		}
	}

	public Connection newConnection() throws SQLException {
		Connection connection = createConnection();

		created.add(connection);

		return connection;
	}

	private Connection createConnection() throws SQLException {
		return 	DriverManager.getConnection(JDBC_URL, USER, PASSWORD);
	}
}
