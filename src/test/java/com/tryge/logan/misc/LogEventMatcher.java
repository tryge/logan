package com.tryge.logan.misc;

import com.tryge.logan.services.LogEvent;
import org.hamcrest.Description;
import org.hamcrest.Factory;
import org.junit.internal.matchers.TypeSafeMatcher;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author michael.zehender@me.com
 */
public class LogEventMatcher extends TypeSafeMatcher<LogEvent> {
	private final Date date;
	private final String level;
	private final String component;
	private final String message;
	private final String body;

	private final List<String> errors = new ArrayList<String>();

	private LogEventMatcher(Date date, String level, String component, String message, String body) {
		this.date = date;
		this.level = level;
		this.component = component;
		this.message = message;
		this.body = body;
	}

	@Override
	public boolean matchesSafely(LogEvent event) {
		errors.clear();

		return checkDate(event)
			& checkLevel(event)
			& checkComponent(event)
			& checkMessage(event)
			& checkBody(event);
	}

	private boolean checkDate(LogEvent event) {
		return check(
			date, event.getDate(), "date"
		);
	}

	private boolean checkLevel(LogEvent event) {
		return check(
			level, event.getLevel(), "level"
		);
	}

	private boolean checkComponent(LogEvent event) {
		return check(
			component, event.getComponent(), "component"
		);
	}

	private boolean checkMessage(LogEvent event) {
		return check(
			message, event.getMessage(), "message"
		);
	}


	private boolean checkBody(LogEvent event) {
		return check(
			body, event.getBody(), "body"
		);
	}

	private boolean check(Object expected, Object given, String name) {
		boolean value = expected == given || (expected != null && expected.equals(given));

		if (!value) {
			errors.add(name + " should be: '" + expected + "' but is: '" + given + "'");
		}

		return value;
	}

	@Override
	public void describeTo(Description description) {
		description.appendText(errors.toString());
	}

	@Factory
	public static LogEventMatcher isEvent(Date date, String level, String component, String message, String body) {
		return new LogEventMatcher(date, level, component, message, body);
	}
}
