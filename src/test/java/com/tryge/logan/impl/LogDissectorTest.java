package com.tryge.logan.impl;

import com.tryge.logan.misc.Utils;
import com.tryge.logan.services.LogDissector;
import com.tryge.logan.services.LogEventProcessor;
import org.junit.Before;
import org.junit.Test;

import java.text.ParseException;

import static com.tryge.logan.misc.LogEventMatcher.isEvent;
import static com.tryge.logan.misc.Utils.parseLog4JDate;
import static org.mockito.Matchers.argThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

/**
 * @author michael.zehender@me.com
 */
public class LogDissectorTest {
	private LogDissector logDissector;
	private LogEventProcessor processor;

	@Before
	public void setUp() {
		processor = mock(LogEventProcessor.class);

		LogDissectorImpl logDissector = new LogDissectorImpl();
		logDissector.setProcessor(processor);
		logDissector.setLineMatcher(
			Utils.getLog4JLineMatcher()
		);
		logDissector.setDateFormat(
			Utils.getLog4JDateFormat()
		);

		this.logDissector = logDissector;
	}

	@Test
	public void singleLineEvents() throws ParseException {
		logDissector.pushLine("2000-09-07 14:07:41,508 [main] DEBUG MyApp - Entering application.");
		logDissector.pushLine("2000-09-07 14:07:41,608 [main] INFO  MyApp - Exiting application.");
		logDissector.eof();

		verify(processor).process(argThat(isEvent(parseLog4JDate("2000-09-07 14:07:41,508"), "DEBUG", "MyApp", "Entering application.", null)));
		verify(processor).process(argThat(isEvent(parseLog4JDate("2000-09-07 14:07:41,608"), "INFO", "MyApp", "Exiting application.", null)));
	}

	@Test
	public void multiLineEvents() throws ParseException {
		logDissector.pushLine("2000-09-07 14:07:41,508 [main] DEBUG MyApp - Entering application.");
		logDissector.pushLine("the debug information for start-up");
		logDissector.pushLine("2000-09-07 14:07:41,608 [main] INFO  MyApp - Exiting application.");
		logDissector.eof();

		verify(processor).process(argThat(isEvent(parseLog4JDate("2000-09-07 14:07:41,508"), "DEBUG", "MyApp", "Entering application.", "the debug information for start-up")));
		verify(processor).process(argThat(isEvent(parseLog4JDate("2000-09-07 14:07:41,608"), "INFO", "MyApp", "Exiting application.", null)));
	}

	@Test
	public void multiLineLastEvents() throws ParseException {
		String debug1 = "the debug information for shutdown 1. line";
		String debug2 = "the debug information for shutdown 2. line";

		logDissector.pushLine("2000-09-07 14:07:41,508 [main] DEBUG MyApp - Entering application.");
		logDissector.pushLine("2000-09-07 14:07:41,608 [main] INFO  MyApp - Exiting application.");
		logDissector.pushLine(debug1);
		logDissector.pushLine(debug2);
		logDissector.eof();

		verify(processor).process(argThat(isEvent(parseLog4JDate("2000-09-07 14:07:41,508"), "DEBUG", "MyApp", "Entering application.", null)));
		verify(processor).process(argThat(isEvent(parseLog4JDate("2000-09-07 14:07:41,608"), "INFO", "MyApp", "Exiting application.", debug1 + "\n" + debug2)));
	}
}
