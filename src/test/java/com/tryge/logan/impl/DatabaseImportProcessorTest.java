package com.tryge.logan.impl;

import com.tryge.logan.misc.Database;
import com.tryge.logan.misc.Utils;
import com.tryge.logan.services.LogEvent;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import javax.sql.DataSource;
import java.io.File;
import java.io.IOException;
import java.sql.*;
import java.text.ParseException;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @author michael.zehender@me.com
 */
public class DatabaseImportProcessorTest {
	@Rule
	public TemporaryFolder temp = new TemporaryFolder();

	@Rule
	public Database database = new Database();

	private DatabaseImportProcessor processor;
	private DataSource dataSource;
	private File file;

	@Before
	public void setUp() throws IOException, SQLException {
		file = temp.newFile();

		dataSource = mock(DataSource.class);

		when(dataSource.getConnection()).thenAnswer(new Answer<Object>() {
			@Override
			public Object answer(InvocationOnMock invocationOnMock) throws Throwable {
				return database.newConnection();
			}
		});

		processor = new DatabaseImportProcessor();
		processor.setDataSource(dataSource);
	}

	@Test
	public void givenSingleLineThenImportIt() throws ParseException, SQLException {
		LogEvent event = new LogEvent();
		event.setDate(Utils.parseLog4JDate("2000-09-07 14:07:41,508"));
		event.setLevel("DEBUG");
		event.setMessage("Entering application");
		event.setComponent("MyApp");
		event.setBody(null);

		processor.file(file);
		processor.process(event);
		processor.eof();

		verifyLogEvent(file, event);
	}

	@Test
	public void givenTwoLinesThenImportThem() throws ParseException, SQLException {
		LogEvent event1 = new LogEvent();
		event1.setDate(Utils.parseLog4JDate("2000-09-07 14:07:41,508"));
		event1.setLevel("DEBUG");
		event1.setMessage("Entering application");
		event1.setComponent("MyApp");
		event1.setBody(null);


		LogEvent event2 = new LogEvent();
		event2.setDate(Utils.parseLog4JDate("2000-09-07 14:07:41,608"));
		event2.setLevel("DEBUG");
		event2.setMessage("Exiting application");
		event2.setComponent("MyApp");
		event2.setBody(null);

		processor.file(file);
		processor.process(event1);
		processor.process(event2);
		processor.eof();

		verifyLogEvent(file, event1);
		verifyLogEvent(file, event2);
	}

	private void verifyLogEvent(File file, LogEvent event) throws SQLException {
		Connection c = database.newConnection();

		PreparedStatement statement = c.prepareStatement("SELECT log_file_id FROM log_files WHERE name = ?");
		statement.setString(1, file.getPath());

		ResultSet resultSet = statement.executeQuery();

		boolean hasNext = resultSet.next();
		assertTrue(hasNext);

		int logFileId = resultSet.getInt("log_file_id");
		assertFalse(logFileId < 0);

		resultSet.close();
		statement.close();

		statement = c.prepareStatement("SELECT log_event_id FROM log_events WHERE log_file_id = ? AND clock = ? AND component = ? AND level = ? AND message = ? AND "
			+ (event.getBody() == null ? " body is NULL" : " body = ?")
		);
		statement.setInt(1, logFileId);
		statement.setTimestamp(2, new Timestamp(event.getDate().getTime()));
		statement.setString(3, event.getComponent());
		statement.setString(4, event.getLevel());
		statement.setString(5, event.getMessage());

		if(event.getBody() != null) {
			statement.setString(6, event.getBody());
		}

		resultSet = statement.executeQuery();

		hasNext = resultSet.next();
		assertTrue(hasNext);


		int logEventId = resultSet.getInt("log_event_id");
		assertFalse(logEventId < 0);

		resultSet.close();
		statement.close();
	}
}
