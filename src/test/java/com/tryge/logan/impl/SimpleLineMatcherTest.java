package com.tryge.logan.impl;

import com.tryge.logan.services.LineMatcher;
import com.tryge.logan.services.LogLineMatch;
import com.tryge.logan.services.MatchResult;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * @author michael.zehender@me.com
 */
public class SimpleLineMatcherTest {
	private LineMatcher lineMatcher;

	@Before
	public void setUp() {
		LineMatcherImpl lineMatcher = new LineMatcherImpl();
		lineMatcher.setExpression("^\\[(\\w+)\\]: (.*)$");
		lineMatcher.setLevelGroup(1);
		lineMatcher.setMessageGroup(2);

		this.lineMatcher = lineMatcher;
	}

	@Test
	public void noMatch() {
		MatchResult result = lineMatcher.match("no log line");

		assertFalse(result.isMatch());
	}

	@Test
	public void simpleMatch() {
		MatchResult result = lineMatcher.match("[DEBUG]: my message");

		assertTrue(result.isMatch());

		LogLineMatch match = (LogLineMatch)result;
		assertEquals("DEBUG", match.getLevel());
		assertEquals("my message", match.getMessage());
	}
}
