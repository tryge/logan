package com.tryge.logan.impl;

import com.tryge.logan.services.LogDissector;
import com.tryge.logan.services.LogFileReader;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.*;

import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.*;

/**
 * @author michael.zehender@me.com
 */
public class LogFileReaderTest {
	@Rule
	public TemporaryFolder folder = new TemporaryFolder();

	private LogDissector dissector;
	private LogFileReader reader;
	private File logFile;

	@Before
	public void setUp() throws IOException {
		dissector = mock(LogDissector.class);

		LogFileReaderImpl reader = new LogFileReaderImpl();
		reader.setLogDissector(dissector);
		this.reader = reader;

		logFile = folder.newFile();
	}

	@Test(expected = FileNotFoundException.class)
	public void testNonExistingFile() throws IOException {
		boolean deleted = logFile.delete();

		assertTrue("logfile couldn't be deleted.", deleted);

		reader.read(logFile);
	}

	@Test
	public void testEmptyFile() throws IOException {
		reader.read(logFile);

		verify(dissector, never()).pushLine(isA(String.class));
		verify(dissector).eof();
	}

	@Test
	public void testSingleEntryFile() throws IOException {
		PrintStream out = new PrintStream(new FileOutputStream(logFile));
		out.println("Line");
		out.close();

		reader.read(logFile);

		verify(dissector, times(1)).pushLine("Line");
		verify(dissector, atMost(1)).pushLine(isA(String.class));
		verify(dissector).eof();
	}

	@Test
	public void testMultipleEntryFile() throws IOException {
		PrintStream out = new PrintStream(new FileOutputStream(logFile));
		out.println("Line 1");
		out.println("Line 2");
		out.println("Line 3");
		out.println("Line 4");
		out.close();

		reader.read(logFile);

		verify(dissector, times(1)).pushLine("Line 1");
		verify(dissector, times(1)).pushLine("Line 2");
		verify(dissector, times(1)).pushLine("Line 3");
		verify(dissector, times(1)).pushLine("Line 4");
		verify(dissector, atMost(4)).pushLine(isA(String.class));
		verify(dissector).eof();
	}
}
