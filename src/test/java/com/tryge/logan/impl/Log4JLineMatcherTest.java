package com.tryge.logan.impl;

import com.tryge.logan.misc.Utils;
import com.tryge.logan.services.LineMatcher;
import com.tryge.logan.services.LogLineMatch;
import com.tryge.logan.services.MatchResult;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * @author michael.zehender@me.com
 */
public class Log4JLineMatcherTest {
	private LineMatcher lineMatcher;

	@Before
	public void setUp() {
		lineMatcher = Utils.getLog4JLineMatcher();
	}

	@Test
	public void noMatch() {
		MatchResult result = lineMatcher.match("no log line");

		assertFalse(result.isMatch());
	}

	@Test
	public void match() {
		MatchResult result = lineMatcher.match("2000-09-07 14:07:41,508 [main] INFO  MyApp - Entering application.");

		assertTrue(result.isMatch());

		LogLineMatch match = (LogLineMatch)result;
		assertEquals("2000-09-07 14:07:41,508", match.getDate());
		assertEquals("INFO", match.getLevel());
		assertEquals("MyApp", match.getComponent());
		assertEquals("Entering application.", match.getMessage());
	}
}
