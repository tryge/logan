package com.tryge.logan.impl;

import com.tryge.logan.services.ImportProcessor;
import com.tryge.logan.services.LogFileReader;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.mockito.InOrder;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;

import static org.mockito.Mockito.*;

/**
 * @author michael.zehender@tryge.com
 */
public class ImporterTest {
	@Rule
	public TemporaryFolder folder = new TemporaryFolder();

	private ImporterImpl importer;
	private LogFileReader reader;
	private ImportProcessor processor;
	private File file;
	private File nonExisting;

	private InOrder inOrder;

	@Before
	public void setUp() throws IOException {
		reader = mock(LogFileReader.class);
		processor = mock(ImportProcessor.class);
		file = folder.newFile();
		nonExisting = new File(folder.getRoot(), "non-existing-file");

		inOrder = inOrder(reader, processor);

		importer = new ImporterImpl();
		importer.setLogFileReader(reader);
		importer.setImportProcessor(processor);
	}

	@Test
	public void testNonExistingFile() throws IOException {
		importer.importFile(nonExisting);

		inOrder.verifyNoMoreInteractions();
	}

	@Test
	public void testImport() throws IOException {
		importer.importFile(file);

		inOrder.verify(processor).file(file);
		inOrder.verify(reader).read(file);
		inOrder.verify(processor).eof();
		inOrder.verifyNoMoreInteractions();
	}

	@Test(expected = RuntimeException.class)
	public void testImportFailure() throws IOException {
		doThrow(new SQLException("test")).when(reader).read(file);

		importer.importFile(file);
	}
}
